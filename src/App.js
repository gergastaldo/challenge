import React from 'react';
import './App.css';
import UserForm from './components/userForm';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <p>
          Coding Chalenge
        </p>
        <UserForm />
      </header>
    </div>
  );
}

export default App;
