import React, { useState } from 'react';
import MockService from '../services/mockService';
import memoize from 'promise-memoize';

const memoizedCalculate = memoize(MockService.calculate);

function UserForm(props) {
  const [num, setNum] = useState(0);
  const [valueList, setValueList] = useState([]);

  const handleSubmit = async (event) => {
    event.preventDefault();

    let resut = await memoizedCalculate(num);
    const keys = Object.keys(resut);
    setValueList(keys.map(k => ({key: k, value: resut[k]})));
  }

  const handleChange = (ev) => {
    const val = Number(ev.target.value);
    
    if (val >= 0 && val <= 100) {
      setNum(val);
    }
  }

  return (
    <form onSubmit={handleSubmit}>
      <input type="text" value={num} onChange={handleChange} />
      <input type="submit" value="Calculate" />
      <br/>
      <div>
        <ul>
          {valueList.map((item) => 
            <li key={item.key}>
              <b>{item.key}:</b> {JSON.stringify(item.value)}
            </li>
          )}
        </ul>
      </div>
    </form>
  )
}

export default UserForm;
