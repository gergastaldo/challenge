let lastDate = null;
let occurrencesMap = new Map();

export default class MockService {
  static calculate(num) {
    return new Promise((resolve, reject) => {
      let sumSquares = 0, sumNaturals = 0;
      for (let i = 1; i <= num; i++) {
        sumSquares += i * i;
        sumNaturals += i;
      }
      const result = (sumNaturals * sumNaturals) - sumSquares;

      if (occurrencesMap.has(num)) {
        occurrencesMap.set(num, occurrencesMap.get(num) + 1)
      }
      else{
        occurrencesMap.set(num, 1);
      }

      resolve({
        "datetime": new Date(),
        "value": result,
        "number": num,
        "occurrences": occurrencesMap.get(num), // the number of times n has been requested
        "last_datetime": lastDate
      });
      lastDate = new Date();
    })
  }
}
